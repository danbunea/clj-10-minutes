(ns clj-10-min-server.handler
  (:require [compojure.api.sweet :refer :all]
            [ring.util.http-response :refer :all]
            [schema.core :as s]
            [environ.core :refer [env]]
            [ring.adapter.jetty :as jetty]
            ))



(def dan {
           :name "Dan"
           :address {
                      :city "Timisoara, Romania"
                      }})

(def app
  (api
    {:swagger
     {:ui "/"
      :spec "/swagger.json"
      :data {:info {:title "Clj-10-min-server"
                    :description "Clojure in 10 minutes Api example"}}}}

    (context "/api" []
             :tags ["api"]

             (GET "/dan" []
                  (ok dan))


             (GET "/move" []
                  :query-params [city :- s/Str
                                 street :- s/Str
                                 no :- s/Num
                                 ]
                  (-> dan
                      (assoc-in [:address :city] city)
                      (assoc-in [:address :street] street)
                      (assoc-in [:address :no] no)
                      (ok )))

             )))




(def app-port (or (env :port) 3000))

(defn -main [& [port-override]]
  (let [port (Integer. (or port-override app-port))]
    (jetty/run-jetty app {:port port :join? false})))

