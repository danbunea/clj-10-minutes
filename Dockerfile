FROM clojure:lein-2.7.1-alpine
COPY target/server.jar /server.jar
ENTRYPOINT ["java", "-jar", "server.jar"]
