# clj-10-min-server

FIXME

## Usage

### Run the application locally

`lein ring server`

### Run the tests

`lein test`

### Packaging and running as standalone jar

```
lein do clean, ring uberjar
java -jar target/server.jar
```
or

java -cp target/server.jar clojure.main -m clj-10-min-server.handler

### Packaging as war

`lein ring uberwar`

## License

Copyright ©  FIXME
