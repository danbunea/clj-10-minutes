(ns clj-10-min-server.core-test
  (:require [cheshire.core :as cheshire]
            [clojure.test :refer :all]
            [clj-10-min-server.handler :refer :all]
            [ring.mock.request :as mock]))

(defn parse-body [body]
  (cheshire/parse-string (slurp body) true))

;; (deftest a-test
;;     (let [response (app (-> (mock/request :get  "/api/change-city?city=Barcelona")))
;;           body     (parse-body (:body response))]
;;       (is (= (:status response) 200))
;;       (is (= (-> body :address :city) "Barcelona"))))
